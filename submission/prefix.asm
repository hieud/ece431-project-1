.macro intToChar(%intA, %isNum, %charA)
	la		$t0, intArray			# load int array
	la		$t1, isNumber		# load isNumber
	la		$t2, %charA		# load char array	
    li      $t6, 0
    sb      $t6, ($t2)
loopIntToChar:
	lw		$t4, ($t0)		#	$t4 = intA[i]
    # print_int_ln    $t0
	beq		$t4, $zero, endIntToChar	# if $t4 == $zero then endIntToChar
	lb		$t5, ($t1)		# 	$t5 = isNumber[i]
	lb		$t6, ($t2)		#   $t6 = charA[i]
    # print_endl
    # print_const_str "address of charA: "
    # print_int_ln    $t2
    # print_const_str "address of int[i]: "
    # print_int_ln    $t0
    # print_const_str "isNumber[i]: "
    # print_int_ln    $t5
    print_const_str "intA[i]: "
	print_int_ln    $t4
    # print_const_str "charA[i]: "
    # print_int_ln    $t6
    # print_const_str "charA: "
    # print_str_ln    %charA
	beq		$t5, 1, nextTokenIsOperator	# if $t5 == 1 then nextTokenIsOperator
	j		nextTokenIsNumber				# jump to nextTokenIsNumber
nextTokenIsNumber:
    addi	$sp, $sp, -28			# $sp = $sp + -28
    sw		$t0, 0($sp)		# 
    sw		$t1, 4($sp)		# 
    sw		$t2, 8($sp)		# 
    sw		$t3, 12($sp)		# 
    sw		$t4, 16($sp)		# 
    sw		$t5, 20($sp)	   	# 
    sw		$t6, 24($sp)		#   
    # print_str_ln    %charA
	appendNumberToString	$t4, %charA
    # print_str_ln    %charA

    lw		$t0, 0($sp)		# 
    lw		$t1, 4($sp)		# 
    lw		$t2, 8($sp)		# 
    lw		$t3, 12($sp)		# 
    lw		$t4, 16($sp)		# 
    lw		$t5, 20($sp)		# 
    lw		$t6, 24($sp)		#   
    addi	$sp, $sp, 28			# $sp = $sp + 28
    # print_const_str "numberSize: "
    # print_int_ln    $v0
	add		$t2, $t2, $v0		# update charArray pointer to the next byte
	j		continueLoopIntToChar
nextTokenIsOperator:
	sb		$t4, ($t2)		# charA[i] = $t4
    li		$t5, 0		# $t5 = 0
    sb      $t5, 1($t2)
    # print_int_ln    $t4
    # print_int_ln    $t5	
	add		$t2, $t2, 1		# update charArray pointer to the next byte	
	j		continueLoopIntToChar
continueLoopIntToChar:
	addi	$t0, $t0, 4			# $t0 = $t0 + 4
	addi	$t1, $t1, 1			# $t1 = $t1 + 1
    # print_const_str "charA after inserting: "
    # print_str_ln    %charA
    # print_endl
	j		loopIntToChar				# jump to loopIntToChar
endIntToChar:
	li		$t7, 0		# $t7 = 0
	sb		$t7, 0($t2)		# 
.end_macro

.macro charToInt(%charA, %intA, %isNumber)
    #convert a char array stored in %charA to an int array stored in %intA with %isNumber to save if the corresponding element in %intA is a number or a character
    addi	$sp, $sp, -24			# $sp = $sp - 24
    sw		$s0, 0($sp)		# 
    sw		$s1, 4($sp)		# 
    sw		$s2, 8($sp)		# 
    sw		$s3, 12($sp)		# 
    sw		$s4, 16($sp)		# 
    sw		$s5, 20($sp)		# 

    # la		$t0, %charA		# load char array
    la		$t1, %intA		# load int array
    la		$t2, %isNumber		# load isNumber
    li		$s0, 0		# $s0 = 0
    li		$s1, 1		# $s1 = 1
    li		$s2, 1		# $s2 = 1   
loopCharToInt:
    la		$t0, %charA		# load char array 
    add		$t0, $t0, $s0		# $t0 = $t0 + $s0
    lb		$s3, 0($t0)		# 
    beq		$s3, $zero, endCharToInt	# if $s3 == $zero then endCharToInt
        
    addi	$sp, $sp, -8			# $sp = $sp + 8
    sw		$t1, 0($sp)		# store ADD[intA]
    sw		$t2, 4($sp)		# store ADD[isNumber]
    
    getToken    %charA, $s0, $s1, $s2
    print_endl
    print_int_ln    $v0
    print_int_ln    $v1
    print_endl

    lw		$t1, 0($sp)		# 
    lw		$t2, 4($sp)		# 
    addi	$sp, $sp, 8			# $sp = $sp + 8

    beq		$v0, 0, errorCharToInt	# if $v0 == 0 then errorCharToInt
#     beq		$v0, 1, isOperatorCharToInt	# if $v0 == 1 then isOperatorCharToInt

# isNumberCharToInt:
#     li		$t3, 2		# $t3 = 2
#     j		continueLoopCharToInt				# jump to continueLoopCharToInt
# isOperatorCharToInt:
#     li		$t3, 1		# $t3 = 1
#     j		continueLoopCharToInt				# jump to continueLoopCharToInt
continueLoopCharToInt:
    # print_int_ln    $v0
    # print_int_ln    $v1
    # print_const_str "-----\n"
    sb		$v0, 0($t2)		# isNumber[i] = $t3 (can be 1 or 0)
    sw		$v1, 0($t1)		# intA[i] = $v1

    lb      $t4, 0($t2)
    lw      $t5, 0($t1)
    # print_int_ln    $t4
    # print_int_ln    $t5
    # print_endl

    add	    $s0, $s0, $s2			# $s0 = $s0 + $s2    
    addi	$t1, $t1, 4			# $t1 = $t1 + 4
    addi	$t2, $t2, 1			# $t2 = $t2 + 1
    
    
    # lb		$s4, 0($t1)		# 
    # lb		$s5, 0($t2)		# 
    j		loopCharToInt				# jump to loopCharToInt
errorCharToInt:
    print_const_str "Error in CharToInt"
endCharToInt:
    li		$t3, 0		# $t3 = 0
    sb		$t3, 0($t2)		# add '\0' to end of isNumber array
    sw		$t3, 0($t1)		# add '\0' to end of intA array
    
    # restore s0 -> s5
    lw		$s0, 0($sp)		# 
    lw		$s1, 4($sp)		# 
    lw		$s2, 8($sp)		# 
    lw		$s3, 12($sp)		# 
    lw		$s4, 16($sp)		# 
    lw		$s5, 20($sp)		# 
    addi	$sp, $sp, 24			# $sp = $sp + 24
.end_macro

.macro printIntArray (%array, %isNumber)
    la		$t0, %array		# load array
    la		$t1, %isNumber		# load isNumber
loopPrintIntArray:
    lw		$t2, 0($t0)		# load array[i]
    # print_int_ln    $t2
    beq		$t2, $zero, endPrintIntArray	# if $t2 == $zero then endPrintIntArray
    lb		$t3, 0($t1)		# load isNumber[i]
    beq		$t3, 1, isOperatorPrintIntArray	# if $t3 == 1 then isOperatorPrintIntArray
isNumberPrintIntArray:
    print_const_str "Number: "
    j		continueLoopPrintIntArray				# jump to continueLoopPrintIntArray
isOperatorPrintIntArray:
    print_const_str "Operator: "
continueLoopPrintIntArray:
    print_int_ln    $t2
    addi	$t0, $t0, 4			# $t0 = $t0 + 4
    addi	$t1, $t1, 1			# $t1 = $t1 + 1
    j		loopPrintIntArray				# jump to loopPrintIntArray
endPrintIntArray:
.end_macro

.macro evaluatePrefix(%expression)
	addi $sp, $sp, -24
	sw $s0, 0($sp)
	sw $s1, 4($sp)
	sw $s2, 8($sp)
	sw $s3, 12($sp)
	sw $s4, 16($sp)
	sw $s5, 20($sp)
	li		$s0, 0		# load ADD[expression] 
	# add		$s3, $sp, $zero		# $s3 = original $sp
	li		$s1, 1		# $s1 = 1
	li		$s2, 1		# $s2 = 1
loopEvaluatePrefix:
	getToken	%expression, $s0, $s1, $s2
	beq		$v0, $zero, endEvaluatePrefix	# if $v0 == $zero then endEvaluatePrefix
	beq		$v0, 1, tokenIsOperator	# if $v0 == 1 then tokenIsOperator
	j		tokenIsOperand				# jump to tokenIsOperand
tokenIsOperand:
	#print_const_str	"Operand\n"
	addi	$sp, $sp, -4			# $sp = $sp - 4
	sw		$v1, ($sp)		# push $v1 to stack
	j 	continueLoop
tokenIsOperator:
	#print_const_str	"Operator\n"
	lw		$s4, ($sp)		# $s4 = a
	lw		$s3, 4($sp)		# $s3 = b
	addi	$sp, $sp, 8			# $sp = $sp + 8
	
	# +
	beq		$v1, 43, operatorIsPlus	# if $v1 == 43 then operatorIsPlus
	# -
	beq		$v1, 45, operatorIsMinus	# if $v1 == 45 then operatorIsMinus
	# *
	beq		$v1, 42, operatorIsAsterisk	# if $v1 == 42 then operatorIsAsterisk
	# /
	beq		$v1, 47, operatorIsSlash	# if $v1 == 47 then operatorIsSlash
	
	print_const_str "No Operator Found\n"
	done
operatorIsPlus:
	#print_const_str "+\n"
	add		$s5, $s4, $s3		# $s5 = $s4 + $s3
	j		pushResultToStack				# jump to pushResultToStack
operatorIsMinus:
	#print_const_str "-\n"
	sub		$s5, $s4, $s3		# $s5 = $s4 - $s3
	j		pushResultToStack				# jump to pushResultToStack
operatorIsAsterisk:
	#print_const_str "*\n"
	mul		$s5, $s4, $s3		# $s5 = $s4 * $s3
	j		pushResultToStack				# jump to pushResultToStack
operatorIsSlash:
	#print_const_str "\\\n"
	div		$s5, $s4, $s3		# $s5 = $s4 / $s3
pushResultToStack:	
	add		$sp, $sp, -4		# $sp = $sp + 4
	#print_const_str "Result: "
	#print_int_ln	$s5
	sw		$s5, ($sp)		# push $s5 to top of the stack	
continueLoop:
	add		$s0, $s0, $s2		# $t0 = $t0 + $s2
	j		loopEvaluatePrefix				# jump to loopEvaluatePrefix
endEvaluatePrefix:
	lw		$v0, ($sp)		# return top of the stack
	addi	$sp, $sp, 4
	#print_int_ln	$v0
	lw $s0, 0($sp)
	lw $s1, 4($sp)
	lw $s2, 8($sp)
	lw $s3, 12($sp)
	lw $s4, 16($sp)
	lw $s5, 20($sp)
	
	addi $sp, $sp, 24
.end_macro

.macro postFixModded(%word, %word_size)
	resetCharArray temp4
	addi	$t0, %word_size, -1 #start of postFixModded
	print_const_str "word_size: "
	print_int $t0
	print_const_str "\n"
	li	$t1, 0	# index of word
	li	$t2, 1	# numberIsNext
	add $t6, $sp, $zero
loopWordPostFixModded:
	addi $sp, $sp, -16
	sw $t0, 0($sp)
	sw $t1, 4($sp)
	sw $t6, 8($sp)
	sw $t5, 12($sp)
	#sw $t2, 8($sp)
	#sw $t3, 12($sp)
	getToken	%word, $t1, $t2, $t3	#t3 is size of token
	lw $t0, 0($sp)
	lw $t1, 4($sp)
	lw $t6, 8($sp)
	lw $t5, 12($sp)
	addi $sp, $sp, 16
	add $t1, $t1, $t3

	beq $v0, $zero, endLoopWordPostFixModded	#branch to end
	beq $v0, 1, OperatorPostFixModded		#branch if token is operator
	# Operand
	addi $sp, $sp, -16
	sw $t0, 0($sp)
	sw $t1, 4($sp)
	sw $t2, 8($sp)
	sw $t3, 12($sp)
	appendNumberToString $v1, temp4
	appendStringToString temp4, space
	lw $t0, 0($sp)
	lw $t1, 4($sp)
	lw $t2, 8($sp)
	lw $t3, 12($sp)
	addi $sp, $sp, 16
	j loopWordPostFixModded

OperatorPostFixModded:
	
	# (
	beq 	$v1, 40, IsLeftBracketPostFixModded
	# )
	beq		$v1, 41, IsRightBracketPostFixModded
	# +
	beq		$v1, 43, IsComputeOpPostFixModded	# if $v1 == 43 then operatorIsPlus
	# -
	beq		$v1, 45, IsComputeOpPostFixModded	# if $v1 == 45 then operatorIsMinus
	# *
	beq		$v1, 42, IsComputeOpPostFixModded	# if $v1 == 42 then operatorIsAsterisk
	# /
	beq		$v1, 47, IsComputeOpPostFixModded	# if $v1 == 47 then operatorIsSlash

	print_const_str "No OperatorPostFixModded Found\n"
	done

IsLeftBracketPostFixModded:	# push to stack 
	addi	$sp, $sp, -4			# $sp = $sp - 4
	sw		$v1, ($sp)		# push $v1 to stack
	j loopWordPostFixModded

IsRightBracketPostFixModded:	# pop until see left bracket
loopPopPostFixModded:
	beq $t6, $sp, endLoopPopPostFixModded
	lb  $t5, 0($sp)		# pop 
	add $sp, $sp, 4			# update sp
	beq $t5, 40, endLoopPopPostFixModded
	addi $sp, $sp, -16
	sw $t0, 0($sp)
	sw $t1, 4($sp)
	sw $t2, 8($sp)
	sw $t3, 12($sp)
	appendCharToString $t5, temp4
	appendStringToString temp4, space
	lw $t0, 0($sp)
	lw $t1, 4($sp)
	lw $t2, 8($sp)
	lw $t3, 12($sp)
	addi $sp, $sp, 16
	j loopPopPostFixModded
endLoopPopPostFixModded:
	j loopWordPostFixModded

IsComputeOpPostFixModded:
	beq $t6, $sp, pushStackPostFixModded
loopPriorityPostFixModded:
	lb	$t5, 0($sp)
	priority $t5
	add $t5, $zero, $v0
	priority $v1
	add $t7, $zero, $v0
	bgt $t7, $t5, pushStackPostFixModded	# if $t7 > $t5 then pushStackPostFixModded
	lb $t5, 0($sp)
	addi $sp, $sp, 4
	
	addi $sp, $sp, -16
	sw $t0, 0($sp)
	sw $t1, 4($sp)
	sw $t2, 8($sp)
	sw $t3, 12($sp)
	appendCharToString $t5, temp4
	appendStringToString temp4, space
	lw $t0, 0($sp)
	lw $t1, 4($sp)
	lw $t2, 8($sp)
	lw $t3, 12($sp)
	addi $sp, $sp, 16
	j loopPriorityPostFixModded	
pushStackPostFixModded:
	addi $sp, $sp, -4
	sw $v1, 0($sp)
	j loopWordPostFixModded

endLoopWordPostFixModded:
loopPopPostFixModded_final:
	beq $t6, $sp, endLoopPopPostFixModded_final
	lb $t5, 0($sp)		# pop 
	add $sp, $sp, 4		# update sp
	beq $t5, 40, endLoopPopPostFixModded_final
	
	addi $sp, $sp, -16
	sw $t0, 0($sp)
	sw $t1, 4($sp)
	sw $t2, 8($sp)
	sw $t3, 12($sp)
	appendCharToString $t5, temp4
	appendStringToString temp4, space
	lw $t0, 0($sp)
	lw $t1, 4($sp)
	lw $t2, 8($sp)
	lw $t3, 12($sp)
	addi $sp, $sp, 16
	j loopPopPostFixModded_final
endLoopPopPostFixModded_final:
	trimSpace temp4
	# appendStringToString %result, temp4
	# print_str_ln 	temp4
	# evaluatePostfix temp4
	# appendNumberToString $v0, result
	# appendStringToString result, endLine
.end_macro

.macro preFix(%expression, %resultExp, %result)
    addi	$sp, $sp, -4			# $sp = $sp + -4        
    sw		$s0, 0($sp)		# 
    
    charToInt %expression, intArray, isNumber
    printIntArray   intArray, isNumber
    # reverseString   isNumber, isNumberReversed
    # reverseWordString   intArray, intArrayReversed
    # intToChar   intArrayReversed, isNumberReversed, %expression
    # switchBracket %expression
    # getSize %expression
    # addi	$s0, $v0, 0			# $s0 = $v0 + 0
    # postFixModded %expression, $s0
    # evaluatePrefix  temp4
    # appendNumberToString $v0, result
    # appendStringToString    result, endLine
    # charToInt           temp4, intArray, isNumber
    # reverseWordString   intArray, intArrayReversed
    # reverseString       isNumber, isNumberReversed
    # intToChar           intArrayReversed, isNumberReversed, %expression
    # appendStringToString        %result, %expression
    # appendStringToString        %result, endLine
.end_macro