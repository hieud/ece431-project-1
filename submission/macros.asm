.macro readLinebyLine (%x, %buffer)

.data
	file_loc: .asciiz %x
	new_line: .asciiz "\n"  #where would I actually use this?

	#error strings
	readErrorMsg: .asciiz "\nError in reading file\n"
	openErrorMsg: .asciiz "\nError in opening file\n"

.text
openFile:
	#Open file for for reading purposes
	li $v0, 13          #syscall 13 - open file
	la $a0, file_loc    #passing in file name
	li $a1, 0               #set to read mode
	li $a2, 0               #mode is ignored
	syscall
	bltz $v0, openError     #if $v0 is less than 0, there is an error found
	move $s0, $v0           #else save the file descriptor

	#Read input from file
	li $v0, 14          #syscall 14 - read filea
	move $a0, $s0           #sets $a0 to file descriptor
	la $a1, %buffer         #stores read info into buffer
	li $a2, 1024            #hardcoded size of buffer
	syscall             
	bltz $v0, readError     #if error it will go to read error

	#li $v0, 4
	#la $a0, %buffer
	#syscall

	#Close the file 
	li   $v0, 16       # system call for close file
	move $a0, $s0      # file descriptor to close
	syscall            # close file
	
	
	j endProgram

openError:
	la $a0, openErrorMsg
	li $v0, 4
	syscall
	j endProgram

readError:
	la $a0, readErrorMsg
	li $v0, 4
	syscall
	j endProgram

endProgram:
	#li $v0, 10
	#syscall
	
.end_macro

.macro print_int (%x)
	addi	$sp, $sp, -16
	sw	$v0, 0($sp)
	sw	$a0, 4($sp)
	sw	%x, 8($sp)	# store original value of %x
	sw	$t0, 12($sp)	# store original value of $t0
	add	$t0, %x, $zero	# load value of %x to $t0
	
	# print %x
	li	$v0, 1
	addi	$a0, $t0, 0
	syscall
	
	lw	$v0, 0($sp)
	lw	$a0, 4($sp)
	lw 	%x, 8($sp)
	lw	$t0, 12($sp)
	addi	$sp, $sp, 16
	#add $a0, $zero, %x
.end_macro
	
.macro print_const_str (%str)
	.data
myLabel: 	.asciiz %str
	.text
	addi	$sp, $sp, -8
	sw		$v0, 0($sp)
	sw		$a0, 4($sp)

	li 		$v0, 4
	la 		$a0, myLabel

	syscall
	lw		$v0, 0($sp)
	lw		$a0, 4($sp)
	addi	$sp, $sp, 8
.end_macro

.macro	print_endl
	print_const_str	"\n"
.end_macro

.macro print_str (%str)
	addi	$sp, $sp, -8
	sw	$v0, 0($sp)
	sw	$a0, 4($sp)
	li $v0, 4
	la $a0, %str
	syscall	
	lw	$v0, 0($sp)
	lw	$a0, 4($sp)
	addi	$sp, $sp, 8
.end_macro

.macro print_str_ln (%str)
	print_str %str
	
	print_const_str "\n"
.end_macro

.macro print_int_ln (%number)
	print_int	%number
	print_const_str "\n"
.end_macro
	
# generic looping mechanism
.macro for (%regIterator, %from, %to, %bodyMacroName)
	add %regIterator, $zero, %from
	Loop:
	%bodyMacroName ()
	add %regIterator, %regIterator, 1
	ble %regIterator, %to, Loop
.end_macro
	
#print an integer
.macro body()
	print_int $t0
	print_const_str "\n"
.end_macro

#exit
.macro done ()
	li $v0,10
	syscall
.end_macro


# get the next word from buffer
.macro getWord(%buffer_address, %word, %wordSize)
	li 	%wordSize, 0
	addi	$t0, %buffer_address, 0
	la 	$t1, %word
	li 	%wordSize, 0
	
	lb	$t2, 0($t0)
	li	$t3, 0
	sltu	$t6, $t3, $t2	# t4 = 1 if t2 > null
	
loopGetWord:
	lb $t2, 0($t0)	# load buffer
	#print_int	$t2
	#print_const_str	"\n"
	
	li $t3, 42
	beq $t2, $t3, okayGetWord	# if t2 is '*'
	
	li $t3, 43
	beq $t2, $t3, okayGetWord	# if t2 is '+'
	
	li $t3, 45
	beq $t2, $t3, okayGetWord	# if t2 is '-'
	
	li $t3, 47
	beq $t2, $t3, okayGetWord	# if t2 is '/'
	
	li $t3, 40
	beq $t2, $t3, okayGetWord	# if t2 is '('
	
	li $t3, 41
	beq $t2, $t3, okayGetWord	# if t2 is ')'
	
	sltiu	$t3, $t2, 58		# t3 = 1 if t2 <= '9'
	li 	$t4, 47
	slt	$t5, $t4, $t2		# t5 = 1 if '0' <= t2
	
	and	$t3, $t3, $t5		# t3 = t3 & t5
	bne	$t3, $zero, okayGetWord
	
	sltiu	$t3, $t2, 91		# t3 = 1 if t2 <= 'Z'
	li 	$t4, 64
	slt	$t5, $t4, $t2		# t5 = 1 if '0' <= t2
	
	and	$t3, $t3, $t5		# t3 = t3 & t5
	bne	$t3, $zero, okayGetWord
	
	sltiu	$t3, $t2, 123		# t3 = 1 if t2 < 'z'
	li 	$t4, 96
	slt	$t5, $t4, $t2		# t4 = 1 if 'a' < t2
	
	and	$t3, $t3, $t5		# t3 = t3 & t5
	bne	$t3, $zero, okayGetWord
	
	j endGetWord
okayGetWord:
	sb $t2, 0($t1)
	
	addi $t0, $t0, 1
	addi $t1, $t1, 1
	addi %wordSize, %wordSize, 1
	j loopGetWord
endGetWord:
	add	%wordSize, %wordSize, $t6
	addi	$v0, %wordSize, 0
	li $t7, 0
	sb $t7, 0($t1)
	#print_int	%wordSize
.end_macro


.macro getToken(%word, %token_index, %number_is_next, %token_size)
	li $t2, 0	#t2 = 0 to save the token_size	
	la $t0, %word	# load address[word] to t0
	add $t0, $t0, %token_index	# increase with index
	li $t3, 0	# t3 is sign register, t3 = 0 when the number is positive 
	li $t4, 0	# t4 = 0, use to track is read number or not
	li $t5, 0	# t5= 0, use to store the number
	add $t6, $zero, %number_is_next
loopGetToken:
	lb $t1, 0($t0)	# load byte to t1
	beq $t1, $zero, failGetTokenLn
	beq $t1, 10, failGetTokenLn
	
	beq $t1, 32, okayGetSpace
	
	addi $v1, $zero, 40	# '('
	addi $t6, $zero, 1	# numberIsNext turn on 
	beq $t1, 40, okayGetChar
	
	add $t6, $zero, %number_is_next
	addi $v1, $zero, 41	# ')'
	beq $t1, 41, okayGetChar
		
	addi $v1, $zero, 42	# '*'
	beq $t1, 42, okayGetChar
	
	addi $v1, $zero, 47	# '/'
	beq $t1, 47, okayGetChar
	
	addi $v1, $zero, 43	# '+'
	beq $t1, 43, okayGetChar
	
	bnez $t6, numberIsNext
	addi $v1, $zero, 45	# '-'
	beq $t1, 45, okayGetChar
	
numberIsNext:
	bne $t1, 45, positive
	nor $t3, $t3, $zero
	add $t2, $t2, 1
	j updateLoop
positive: 
	li $t4, 1  # number is read
	subi $t1, $t1, 48  
	mul $t5, $t5, 10
	add $t5, $t5, $t1
	add $t2, $t2, 1
updateLoop:
	addi $t0, $t0, 1
	j loopGetToken
okayGetSpace:
	beqz $t4, spaceBeforeNum	#check is read number or not, if not, read next token
	addi $t2, $t2, 1
	j okayGetNumber
spaceBeforeNum:
	add $t2, $t2, 1
	j updateLoop
okayGetChar:
	bnez $t4, okayGetNumber		#check is read number or not
	beq $v1, 40, isSignAndBracket	#check sign or bracket
	beq $v1, 45, isSignAndBracket	#check sign or bracket
	li $t6, 0
isSignAndBracket:
	li $v0, 1
	addi $t2, $t2, 1
	j endGetToken
okayGetNumber:
	add $v1, $t5, $zero
	li $v0, 2
	li $t6, 0
	j endGetToken
failGetTokenLn:
	bnez $t4, okayGetNumber	# check is read number or not
failGetToken:
	li $v0, 0
	li $t2, 1
	j endGetToken
endGetToken:
	beqz $t3, thatIsPos	# check negative or positive
	mul $v1, $v1, -1
thatIsPos:
	addi %token_size, $t2, 0
	addi %number_is_next, $t6, 0
.end_macro
	
.macro	getSize(%array)
# To get the size of a space array in which the address is saved
	la	$t0, %array
	li	$t1, 0		# count = 0
loopGetSize:
	lb	$t2, 0($t0)	# load the first byte from the string
	# print_const_str	"array[i]: "
	# print_int_ln	$t2
	beq	$t2, $zero, endGetSize	# if end of array then escape
	addi	$t1, $t1, 1	# count += 1
	addi	$t0, $t0, 1	# go to the next byte in %array
	j 	loopGetSize
endGetSize:
	# print_const_str "answer getSize: "
	# print_int_ln	$t1
	addi	$v0, $t1, 0
.end_macro

.macro	reverseString(%original, %reverse)
# Reverse the string stored in %original and save it in %reverse
	getSize	%original
#	print_str_ln	%original
	addi	$t0, $v0, 0	# t0 = sizeof %original
	#print_int	$v0
	#print_endl
	la	$t1, %original	# t1 = address of original
	la	$t2, %reverse	# t2 = address of reverse
	addi	$t3, $t0, 0	# i = n
	
	add	$t1, $t1, $t0	# t1 = ADD[original] + sizeof %original
	beq	$t0, $zero, loopReverseString
	addi	$t1, $t1, -1	# t1 = ADD[original] + sizeof %original - 1
loopReverseString:
	#print_str_ln	%original
	beq	$t3, $zero, endReverseString	# if i == 0: endReverseString
	lb	$t4, ($t1)	# t4 = original[i - 1]
	#print_int	$t4
	#print_endl
	sb	$t4, ($t2)	# reverse[n - i] = t4	
	addi	$t1, $t1, -1	# move to original[i - 2]
	addi	$t2, $t2, 1	# move to reverse[n - i + 1]
	addi	$t3, $t3, -1	# i -= 1
	j	loopReverseString
endReverseString:	
	#addi	$t2, $t2, 1	# t2 = ADD[original + n]
	li	$t5, 0
	sb	$t5, ($t2)	# ADD[original + n] = '\0'
#	print_str_ln	%reverse
.end_macro

.macro print_string_to_file(%string, %fileName)
	# Open (for writing) a file that does not exist
	li   $v0, 13		# system call for open file
	la   $a0, %fileName	# output file name
	li   $a1, 1       	# Open for writing (flags are 0: read, 1: write)
	li   $a2, 0        	# mode is ignored
	syscall            	# open a file (file descriptor returned in $v0)
	move $s6, $v0      	# save the file descriptor 
	###############################################################
	# Write to file just opened
	li   $v0, 15       	# system call for write to file
	move $a0, $s6      	# file descriptor 
	la   $a1, %string	#address of buffer from which to write
	li   $a2, 44       	# hardcoded buffer length
	syscall            	# write to file
	###############################################################
	# Close the file 
	li   $v0, 16       	# system call for close file
	move $a0, $s6      	# file descriptor to close
	syscall            	# close file
.end_macro

.macro numberToString(%number, %string)
# convert a number to string and append it to the end of %string
	addi	$sp, $sp, -4
	sw		%number, ($sp)
	la		$t0, temp1	# t0 = ADD[%string]
	li		$t1, 10		# t1 = 10
	addi	$t3, %number, 0	# t3 = number
	remu	$t2, $t3, $t1	# t2 = t3 % 10
	# print_const_str	"digit: "
	# print_int_ln	$t2
	beq		$t2, 0, numberIsZero	
loopNumberToString:
	remu	$t2, $t3, $t1	# t2 = t3 % 10	(last digit)
	beq		$t2, $zero, endNumberToString	# if t2 == 0: endNumberToString
	addi	$t2, $t2, 48		# convert t2 to ASCII corresponding symbol
	sb		$t2, ($t0)		# string[i] = t2
	div		$t3, $t3, $t1	# t3 /= 10
	addi	$t0, $t0, 1	# move to next byte in %string
	j	loopNumberToString
numberIsZero:
	li	$t1, 48
	sb	$t1, ($t0)
	addi	$t0, $t0, 1
endNumberToString:
	li	$t1, 0
	sb	$t1, ($t0)
	addi	$sp, $sp, -16			# $sp = $sp + 16
	sw		$t0, 0($sp)		# 
	sw		$t1, 4($sp)		# 
	sw		$t2, 8($sp)		# 
	sw		$t3, 12($sp)		# 
	#print_str_ln	temp
#	print_str_ln	temp1
	# print_str_ln	%string
	reverseString	temp1, %string
	# print_str_ln	%string
	#print_str_ln	word
	lw		$t0, 0($sp)		# 
	lw		$t1, 4($sp)		# 
	lw		$t2, 8($sp)		# 
	lw		$t3, 12($sp)		# 
	addi	$sp, $sp, 16			# $sp = $sp + 16
	lw	%number, ($sp)
	addi	$sp, $sp, 4
	# print_str_ln	%string
.end_macro

.macro appendStringToString(%first, %second)
	getSize	%first
	la	$t0, %second	# load ADD[secont]
	la	$t3, %first	# load ADD[first]
	add	$t3, $t3, $v0	# t0 = ADD[second + sizeof second]
	
loopSTS:
	lb	$t1, ($t0)	# t1 = second[i]
	beq	$t1, $zero, endSTS	# if t1 == '\0': end
	sb	$t1, ($t3)	# first[i] = t1
	addi	$t0, $t0, 1
	addi	$t3, $t3, 1
	j	loopSTS
endSTS:	
	li	$t4, 0
	sb	$t4, ($t3)	# add '\0' to end of string	
.end_macro

.macro appendNumberToString(%number, %string)
	# print_int_ln	%number
	numberToString		%number, temp2
	# print_const_str "temp2: "
	# print_str_ln	temp2
	getSize				temp2
	# print_const_str "sizeof temp2: "
	# print_int_ln	$v0
	addi	$sp, $sp, -4			# $sp = $sp - 4
	sw		$v0, 0($sp)		# 
	appendStringToString	%string, temp2
	lw		$v0, 0($sp)		# 
	addi	$sp, $sp, 4			# $sp = $sp + 4
	
.end_macro

.macro appendCharToString(%char, %string)
	la			$t0, temp3
	sb			%char, ($t0)
	addi	$t0, $t0, 1			# $t0 = $t0 + 1
	li		$t1, 0		# $t1 = 0
	sb		$t1, 0($t0)
	appendStringToString	%string, temp3
.end_macro

.macro resetCharArray(%string)
	addi $sp, $sp, -8
	sw $s0, ($sp)
	sw $s1, 4($sp)
	
	la $s1, %string
	li $s0, 0
	sb $s0, ($s1)
	
	lw $s0, ($sp)
	lw $s1, 4($sp)
	addi $sp, $sp, 8
.end_macro
.macro trimSpace(%string)
	addi $sp, $sp, 8
	sw $s0, ($sp)
	sw $s1, 4($sp)
	
	la $s0, %string
	getSize %string
	addi $v0, $v0 -1
	add $s0, $s0, $v0
	
	lb $s1, ($s0)
	bne $s1, 32, endTrim
	li $s1, 0
	sb $s1, ($s0)
endTrim:
	lw $s0, ($sp)
	lw $s1, 4($sp)
	addi $sp, $sp, 8
.end_macro

.macro	reverseWordString(%original, %reverse)
# Reverse the string stored in %original and save it in %reverse
	getSizeWordArray %original
#	print_str_ln	%original
	addi	$t0, $v0, 0	
	sll $t0, $t0, 2  # t0 = sizeof %original
	#print_int	$v0
	#print_endl
	la	$t1, %original	# t1 = address of original
	la	$t2, %reverse	# t2 = address of reverse
	addi	$t3, $t0, 0	# i = n
	
	add	$t1, $t1, $t0	# t1 = ADD[original] + sizeof %original
	beq	$t0, $zero, loopReverseWordString
	addi	$t1, $t1, -4	# t1 = ADD[original] + sizeof %original - 1
loopReverseWordString:
	#print_str_ln	%original
	beq	$t3, $zero, endReverseWordString	# if i == 0: endReverseWordString
	lw	$t4, ($t1)	# t4 = original[i - 1]
	#print_int	$t4
	#print_endl
	sw	$t4, ($t2)	# reverse[n - i] = t4	
	addi	$t1, $t1, -4	# move to original[i - 2]
	addi	$t2, $t2, 4	# move to reverse[n - i + 1]
	addi	$t3, $t3, -1	# i -= 1
	j	loopReverseWordString
endReverseWordString:	
	#addi	$t2, $t2, 1	# t2 = ADD[original + n]
	li	$t5, 0
	sw	$t5, ($t2)	# ADD[original + n] = '\0'
#	print_str_ln	%reverse
.end_macro


.macro	getSizeWordArray(%array)
# To get the size of a space array in which the address is saved
	la	$t0, %array
	li	$t1, 0		# count = 0
loopGetSizeWordArray:
	lw	$t2, 0($t0)	# load the first byte from the string
	beq	$t2, $zero, endGetSizeWordArray	# if end of array then escape
	addi	$t1, $t1, 1	# count += 1
	addi	$t0, $t0, 4	# go to the next byte in %array
	j 	loopGetSizeWordArray
endGetSizeWordArray:
	addi	$v0, $t1, 0
.end_macro

.macro switchBracket(%string) 

	addi $sp, $sp, -16
	sw $s0, ($sp)
	sw $s1, 4($sp)
	sw $s2, 8($sp)
	sw $s3, 12($sp)

	la $s0, %string
	getSize %string
	addi $s1, $v0, 0
	li $s2, 0
loopSwitch:
	lb $s3, ($s0)
	beq $s3, 0, endLoopSwitch
	beq $s3, 40, isLeftBracketSwitch
	beq $s3, 41, isRightBracketSwitch
	j updateLoopSwitch
isLeftBracketSwitch:
	li $s3, 41
	sb $s3, ($s0)
	j updateLoopSwitch
isRightBracketSwitch:
	li $s3, 40
	sb $s3, ($s0)
	j updateLoopSwitch
updateLoopSwitch:
	addi $s0, $s0, 1
	j loopSwitch
endLoopSwitch:

	lw $s0, ($sp)
	lw $s1, 4($sp)
	lw $s2, 8($sp)
	lw $s3, 12($sp)
	addi $sp, $sp, 16
.end_macro