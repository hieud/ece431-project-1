.include "macros.asm"
.include "postfix.asm"
.include "prefix.asm"
.data
	input_loc: 		.asciiz	"input.txt"
	post_output_loc:	.asciiz "postfix.txt"
	pre_output_loc:		.asciiz "prefix.txt"
	result_output_loc:	.asciiz "result.txt"
	prefix: 		.asciiz	"Prefix"
	postfix:		.asciiz	"Postfix"
	endLine:		.asciiz "\n"
	space:			.asciiz " "
	prefixResult:  	.space 	1024
	postfixResult: 	.space 	1024
	result:		.space	1024
	buffer: 		.space 	1024
	word:			.space 	1024
	reversed:		.space 	1024
	temp1:			.space 	1024
	temp2:			.space 	1024
	temp3:			.space 	1024
	temp4:			.space  1024
	intArray:		.word	1024
	isNumber:		.space	1024
	isNumberReversed:		.space	1024
	intArrayReversed:		.word	1024
	temp5:			.space	1024
	
.text
	readLinebyLine "input.txt" buffer
	
	# printing the file content to screen
	#li $v0, 4
	#la $a0, buffer
	#syscall
	
	la	$s0, buffer	# s0 = ADD[buffer]
	getWord	$s0, word, $s2	# read the first token
	add	$s0, $s0, $s2
	la	$a0, word	# load address of word into 1st argument
	la	$a1, prefix	# load "Prefix" into 2nd argument
	jal	equalString
	
	bne	$v0, $zero, prefixSection	# if the first token is "Prefix" then jump to prefixSection

postfixSection:	
	print_const_str	"Postfix\n"
	
loopPostfix:
	lb	$s1, 0($s0)	# load buffer to s1.
	beq	$s1, $zero, endLoopPostfix	# check condition
	li	$s2, 0
	getWord $s0, word, $s2 # word and s2 = length of word
	print_str_ln word
	postFix	word, $s2, postfixResult
	li $s7, 10
	appendStringToString postfixResult, endLine
	add	$s0, $s0, $s2 # update buffer
	j loopPostfix
endLoopPostfix:
	print_str_ln	postfixResult
	print_string_to_file postfixResult, post_output_loc
	print_string_to_file result, result_output_loc
	j	endProgram
	
	
########### PREFIX #############
	
prefixSection:
	print_const_str	"Prefix\n"
loopPrefix:
	lb	$s1, 0($s0)	# load buffer to s1.
	beq	$s1, $zero, endLoopPrefix	# check condition
	li	$s2, 0
	getWord $s0, word, $s2 # word and s2 = length of word
	print_str_ln word
	preFix	word, $s2, prefixResult
	li $s7, 10
	# appendStringToString postfixResult, endLine
	add	$s0, $s0, $s2 # update buffer
	# j loopPrefix
endLoopPrefix:
	print_str_ln	prefixResult
	print_string_to_file prefixResult, pre_output_loc
	print_string_to_file result, result_output_loc
	j	endProgram


########### END PROGRAM ##########
endProgram:
	li $v0,10
	syscall
	
############EQUAL_STRING##################
equalString:
	addi	$t0, $a0, 0
	addi	$t1, $a1, 0
loopEqualString:
	lb	$t2, 0($t0)	# t2 = a0[i]
	lb	$t3, 0($t1)	# t3 = a1[i]
	
	bne	$t2, $t3, notEqual
	
	sltiu	$t4, $t2, 1	# t4 = 1 if t2 == null
	sltiu	$t5, $t3, 1	# t5 = 1 if t3 == null
	
	and	$t6, $t4, $t5
	bne	$t6, $zero, equal
	
	addi	$t0, $t0, 1
	addi	$t1, $t1, 1
	j	loopEqualString
	
equal:
	addi	$v0, $zero, 1
	j	endFunction
notEqual:
	addi	$v0, $zero, 0
	#print_int	$v0
	j	endFunction
endFunction:
	jr	$ra

#################################
