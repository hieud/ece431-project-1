.macro intToChar(%intA, %isNum, %charA)
	la		$t0, %intA			# load int array
	la		$t1, %isNum		# load isNumber
	la		$t2, %charA		# load char array	
    li      $t6, 0
    sb      $t6, ($t2)
loopIntToChar:
	lw		$t4, ($t0)		#	$t4 = intA[i]
    # print_int_ln    $t0
	beq		$t4, $zero, endIntToChar	# if $t4 == $zero then endIntToChar
	lb		$t5, ($t1)		# 	$t5 = isNumber[i]
	# lb		$t6, ($t2)		#   $t6 = charA[i]
    # print_endl
    # print_const_str "address of charA: "
    # print_int_ln    $t2
    # print_const_str "address of int[i]: "
    # print_int_ln    $t0
    # print_const_str "isNumber[i]: "
    # print_int_ln    $t5
    # print_const_str "intA[i]: "
	# print_int_ln    $t4
    # print_const_str "charA[i]: "
    # print_int_ln    $t6
    # print_const_str "charA: "
    # print_str_ln    %charA
	beq		$t5, 1, nextTokenIsOperator	# if $t5 == 1 then nextTokenIsOperator
	j		nextTokenIsNumber				# jump to nextTokenIsNumber
nextTokenIsNumber:
    addi	$sp, $sp, -28			# $sp = $sp + -28
    sw		$t0, 0($sp)		# 
    sw		$t1, 4($sp)		# 
    sw		$t2, 8($sp)		# 
    sw		$t3, 12($sp)		# 
    sw		$t4, 16($sp)		# 
    sw		$t5, 20($sp)	   	# 
    sw		$t6, 24($sp)		#   
    # print_str_ln    %charA
	print_debug_int_ln	"t4", $t4
	appendNumberToString	$t4, %charA
    # print_str_ln    %charA

    lw		$t0, 0($sp)		# 
    lw		$t1, 4($sp)		# 
    lw		$t2, 8($sp)		# 
    lw		$t3, 12($sp)		# 
    lw		$t4, 16($sp)		# 
    lw		$t5, 20($sp)		# 
    lw		$t6, 24($sp)		#   
    addi	$sp, $sp, 28			# $sp = $sp + 28
    # print_const_str "numberSize: "
    # print_int_ln    $v0
	add		$t2, $t2, $v0		# update charArray pointer to the next byte
	j		continueLoopIntToChar
nextTokenIsOperator:
	sb		$t4, ($t2)		# charA[i] = $t4
    li		$t5, 0		# $t5 = 0
    sb      $t5, 1($t2)
    # print_int_ln    $t4
    # print_int_ln    $t5	
	add		$t2, $t2, 1		# update charArray pointer to the next byte	
	j		continueLoopIntToChar
continueLoopIntToChar:
	addi	$t0, $t0, 4			# $t0 = $t0 + 4
	addi	$t1, $t1, 1			# $t1 = $t1 + 1
    # print_const_str "charA after inserting: "
    # print_str_ln    %charA
    # print_endl
	j		loopIntToChar				# jump to loopIntToChar
endIntToChar:
	li		$t7, 0		# $t7 = 0
	sb		$t7, 0($t2)		# 
.end_macro

.macro intToCharWithSpace(%intA, %isNum, %charA)
	la		$t0, %intA			# load int array
	la		$t1, %isNum		# load isNumber
	la		$t2, %charA		# load char array	
    li      $t6, 0
    sb      $t6, ($t2)
loopintToCharWithSpace:
	lw		$t4, ($t0)		#	$t4 = intA[i]
    # print_int_ln    $t0
	beq		$t4, $zero, endintToCharWithSpace	# if $t4 == $zero then endintToCharWithSpace
	lb		$t5, ($t1)		# 	$t5 = isNumber[i]
	# lb		$t6, ($t2)		#   $t6 = charA[i]
    # print_endl
    # print_const_str "address of charA: "
    # print_int_ln    $t2
    # print_const_str "address of int[i]: "
    # print_int_ln    $t0
    # print_const_str "isNumber[i]: "
    # print_int_ln    $t5
    # print_const_str "intA[i]: "
	# print_int_ln    $t4
    # print_const_str "charA[i]: "
    # print_int_ln    $t6
    # print_const_str "charA: "
    # print_str_ln    %charA
	beq		$t5, 1, nextTokenIsOperatorWithSpace	# if $t5 == 1 then nextTokenIsOperatorWithSpace
	j		nextTokenIsNumberWithSpace				# jump to nextTokenIsNumberWithSpace
nextTokenIsNumberWithSpace:
    addi	$sp, $sp, -28			# $sp = $sp + -28
    sw		$t0, 0($sp)		# 
    sw		$t1, 4($sp)		# 
    sw		$t2, 8($sp)		# 
    sw		$t3, 12($sp)		# 
    sw		$t4, 16($sp)		# 
    sw		$t5, 20($sp)	   	# 
    sw		$t6, 24($sp)		#   
    # print_debug_int_ln "t4",	$t4
	appendNumberToString	$t4, %charA
    # print_str_ln    %charA

    lw		$t0, 0($sp)		# 
    lw		$t1, 4($sp)		# 
    lw		$t2, 8($sp)		# 
    lw		$t3, 12($sp)		# 
    lw		$t4, 16($sp)		# 
    lw		$t5, 20($sp)		# 
    lw		$t6, 24($sp)		#   
    addi	$sp, $sp, 28			# $sp = $sp + 28
    # print_const_str "numberSize: "
    # print_int_ln    $v0
	add		$t2, $t2, $v0		# update charArray pointer to the next byte
	j		continueLoopintToCharWithSpace
nextTokenIsOperatorWithSpace:
	sb		$t4, ($t2)		# charA[i] = $t4
    li		$t5, 0		# $t5 = 0
    sb      $t5, 1($t2)
    # print_int_ln    $t4
    # print_int_ln    $t5	
	add		$t2, $t2, 1		# update charArray pointer to the next byte	
	j		continueLoopintToCharWithSpace
continueLoopintToCharWithSpace:
	addi	$t0, $t0, 4			# $t0 = $t0 + 4
	addi	$t1, $t1, 1			# $t1 = $t1 + 1
	storeAllT
	appendStringToString	%charA, space
	loadAllT
	addi	$t2, $t2, 1			# $t2 = $t2 + 1
	
    # print_const_str "charA after inserting: "
    # print_str_ln    %charA
    # print_endl
	j		loopintToCharWithSpace				# jump to loopintToCharWithSpace
endintToCharWithSpace:
	li		$t7, 0		# $t7 = 0
	sb		$t7, 0($t2)		# 
.end_macro

.macro 	charToInt(%charA, %intA, %isNumber)
    #convert a char array stored in %charA to an int array stored in %intA with %isNumber to save if the corresponding element in %intA is a number or a character
    # addi	$sp, $sp, -24			# $sp = $sp - 24
    # sw		$s0, 0($sp)		# 
    # sw		$s1, 4($sp)		# 
    # sw		$s2, 8($sp)		# 
    # sw		$s3, 12($sp)		# 
    # sw		$s4, 16($sp)		# 
    # sw		$s5, 20($sp)		# 
	storeAllS
    # la		$t0, %charA		# load char array
    la		$s4, %intA		# load int array
    la		$s5, %isNumber		# load isNumber
    li		$s0, 0		# $s0 = 0
    li		$s1, 1		# $s1 = 1
    li		$s2, 1		# $s2 = 1   
loopCharToInt:
    la		$t0, %charA		# load char array 
    add		$t0, $t0, $s0		# $t0 = $t0 + $s0
    lb		$s3, 0($t0)		# 
    beq		$s3, $zero, endCharToInt	# if $s3 == $zero then endCharToInt
	# print_const_str "before getToken"
    # print_int_ln	$s0
	# print_int_ln	$s2
	# print_endl
    # addi	$sp, $sp, -8			# $sp = $sp + 8
    # sw		$s4, 0($sp)		# store ADD[intA]
    # sw		$s5, 4($sp)		# store ADD[isNumber]
    
    getToken    %charA, $s0, $s1, $s2
	# add		$s0, $s0, $s2			# $s0 = $s0 + $s2
	
	# print_endl
	# print_const_str "after getToken"
	# print_int_ln	$s0
	# print_int_ln	$s2
	# print_endl
    # lw		$s4, 0($sp)		# 
    # lw		$s5, 4($sp)		# 
    # addi	$sp, $sp, 8			# $sp = $sp + 8


	
	# storeAllT
    # print_endl
    # print_int_ln    $v0
    # print_int_ln    $v1
    # print_endl
	# loadAllT
	# add	$s0, $s0, $s2			# $s0 = $s0 + $s2
	
    beq		$v0, 0, errorCharToInt	# if $v0 == 0 then errorCharToInt
#     beq		$v0, 1, isOperatorCharToInt	# if $v0 == 1 then isOperatorCharToInt

# isNumberCharToInt:
#     li		$t3, 2		# $t3 = 2
#     j		continueLoopCharToInt				# jump to continueLoopCharToInt
# isOperatorCharToInt:
#     li		$t3, 1		# $t3 = 1
#     j		continueLoopCharToInt				# jump to continueLoopCharToInt
continueLoopCharToInt:
    # print_int_ln    $v0
    # print_int_ln    $v1
	# print_int_ln    $v0
    # print_int_ln    $v1
    # print_const_str "-----\n"
    sb		$v0, 0($s5)		# isNumber[i] = $t3 (can be 1 or 0)
    sw		$v1, 0($s4)		# intA[i] = $v1

    # lb      $s6, 0($s5)
    # lw      $s7, 0($s4)
    # print_int_ln    $s6
	# print_int_ln    $s7
    # print_endl

    add	    $s0, $s0, $s2			# $s0 = $s0 + $s2    
    addi	$s4, $s4, 4			# $s4 = $s4 + 4
    addi	$s5, $s5, 1			# $s5 = $s5 + 1
			
    j		loopCharToInt				# jump to loopCharToInt
errorCharToInt:
    print_const_str "Error in CharToInt"
	done
endCharToInt:
    li		$t3, 0		# $t3 = 0
    sb		$t3, 0($s5)		# add '\0' to end of isNumber array
    sw		$t3, 0($s4)		# add '\0' to end of intA array
    
    # restore s0 -> s5
    # lw		$s0, 0($sp)		# 
    # lw		$s1, 4($sp)		# 
    # lw		$s2, 8($sp)		# 
    # lw		$s3, 12($sp)		# 
    # lw		$lw, 16($sp)		# 
    # lw		$s5, 20($sp)		# 
    # addi	$sp, $sp, 24			# $sp = $sp + 24
	loadAllS
.end_macro

.macro 	charToIntWithSpace(%charA, %intA, %isNumber)
    #convert a char array stored in %charA to an int array stored in %intA with %isNumber to save if the corresponding element in %intA is a number or a character
    # addi	$sp, $sp, -24			# $sp = $sp - 24
    # sw		$s0, 0($sp)		# 
    # sw		$s1, 4($sp)		# 
    # sw		$s2, 8($sp)		# 
    # sw		$s3, 12($sp)		# 
    # sw		$s4, 16($sp)		# 
    # sw		$s5, 20($sp)		# 
	storeAllS
    # la		$t0, %charA		# load char array
    la		$s4, %intA		# load int array
    la		$s5, %isNumber		# load isNumber
    li		$s0, 0		# $s0 = 0
    li		$s1, 1		# $s1 = 1
    li		$s2, 1		# $s2 = 1   
loopcharToIntWithSpace:
    la		$t0, %charA		# load char array 
    add		$t0, $t0, $s0		# $t0 = $t0 + $s0
    lb		$s3, 0($t0)		# 
    beq		$s3, $zero, endcharToIntWithSpace	# if $s3 == $zero then endcharToIntWithSpace
	# print_const_str "before getToken"
    # print_int_ln	$s0
	# print_int_ln	$s2
	# print_endl
    # addi	$sp, $sp, -8			# $sp = $sp + 8
    # sw		$s4, 0($sp)		# store ADD[intA]
    # sw		$s5, 4($sp)		# store ADD[isNumber]
    
    getTokenPrePost    %charA, $s0, $s1, $s2
	# print_debug_int_ln	"v0", $v0
	# print_debug_int_ln	"v1", $v1
    beq		$v0, 0, endcharToIntWithSpace	# if $v0 == 0 then errorcharToIntWithSpace
#     beq		$v0, 1, isOperatorcharToIntWithSpace	# if $v0 == 1 then isOperatorcharToIntWithSpace

# isNumbercharToIntWithSpace:
#     li		$t3, 2		# $t3 = 2
#     j		continueLoopcharToIntWithSpace				# jump to continueLoopcharToIntWithSpace
# isOperatorcharToIntWithSpace:
#     li		$t3, 1		# $t3 = 1
#     j		continueLoopcharToIntWithSpace				# jump to continueLoopcharToIntWithSpace
continueLoopcharToIntWithSpace:
    # print_int_ln    $v0
    # print_int_ln    $v1
	# print_int_ln    $v0
    # print_int_ln    $v1
    # print_const_str "-----\n"
    sb		$v0, 0($s5)		# isNumber[i] = $t3 (can be 1 or 0)
    sw		$v1, 0($s4)		# intA[i] = $v1

    # lb      $s6, 0($s5)
    # lw      $s7, 0($s4)
    # print_int_ln    $s6
	# print_int_ln    $s7
    # print_endl

    add	    $s0, $s0, $s2			# $s0 = $s0 + $s2    
    addi	$s4, $s4, 4			# $s4 = $s4 + 4
    addi	$s5, $s5, 1			# $s5 = $s5 + 1
			
    j		loopcharToIntWithSpace				# jump to loopcharToIntWithSpace
errorcharToIntWithSpace:
    print_const_str "Error in charToIntWithSpace"
	done
endcharToIntWithSpace:
    li		$t3, 0		# $t3 = 0
    sb		$t3, 0($s5)		# add '\0' to end of isNumber array
    sw		$t3, 0($s4)		# add '\0' to end of intA array
    
    # restore s0 -> s5
    # lw		$s0, 0($sp)		# 
    # lw		$s1, 4($sp)		# 
    # lw		$s2, 8($sp)		# 
    # lw		$s3, 12($sp)		# 
    # lw		$lw, 16($sp)		# 
    # lw		$s5, 20($sp)		# 
    # addi	$sp, $sp, 24			# $sp = $sp + 24
	loadAllS
.end_macro

.macro printIntArray (%array, %isNumber)
	storeAllS
    la		$s0, %array		# load array
    la		$s1, %isNumber		# load isNumber
loopPrintIntArray:
    lw		$s2, 0($s0)		# load array[i]
    # print_int_ln    $s2
    beq		$s2, $zero, endPrintIntArray	# if $s2 == $zero then endPrintIntArray
    lb		$s3, 0($s1)		# load isNumber[i]
    beq		$s3, 1, isOperatorPrintIntArray	# if $s3 == 1 then isOperatorPrintIntArray
isNumberPrintIntArray:
    print_const_str "Number: "
    j		continueLoopPrintIntArray				# jump to continueLoopPrintIntArray
isOperatorPrintIntArray:
    print_const_str "Operator: "
continueLoopPrintIntArray:
    print_int_ln    $s2
    addi	$s0, $s0, 4			# $s0 = $s0 + 4
    addi	$s1, $s1, 1			# $s1 = $s1 + 1
    j		loopPrintIntArray				# jump to loopPrintIntArray
endPrintIntArray:
	print_endl
	loadAllS
.end_macro

.macro evaluatePrefix(%expression)
	storeAllS
	li		$s0, 0		# load ADD[expression] 
	getSize	%expression
	add	$s0, $s0, $v0			# $s0 = $s0 + $v0
	addi $s0, $s0, -1
	addi	$s7, $v0, 0			# $s7 = $v0 + 0
	# add		$s3, $sp, $zero		# $s3 = original $sp
	li		$s1, 1		# $s1 = 1
	li		$s2, 1		# $s2 = 1
	addi	$s6, $sp, 0			# $s6 = $sp + 0
loopEvaluatePrefix:
	getTokenPrePost	%expression, $s0, $s1, $s2
	# print_const_str "value: "
	# print_int_ln	$v1
	# print_const_str "type: "
	# print_int_ln	$v0
	# print_const_str "size: "
	# print_int_ln	$s2
	# print_const_str "number is next: "
	# print_int_ln	$s1
	beq		$s7, $zero, endEvaluatePrefix	# if $v0 == $zero then endEvaluatePrefix
	beq		$v0, 1, tokenIsOperatorEvaluatePrefix	# if $v0 == 1 then tokenIsOperatorEvaluatePrefix
	j		tokenIsOperandEvaluatePrefix				# jump to tokenIsOperandEvaluatePrefix
tokenIsOperandEvaluatePrefix:
	#print_const_str	"Operand\n"
	addi	$sp, $sp, -4			# $sp = $sp - 4
	sw		$v1, ($sp)		# push $v1 to stack
	j 	continueLoopEvaluatePrefix
tokenIsOperatorEvaluatePrefix:
	#print_const_str	"Operator\n"
	lw		$s4, ($sp)		# $s3 = b
	lw		$s3, 4($sp)		# $s4 = a
	addi	$sp, $sp, 8			# $sp = $sp + 8
	
	# +
	beq		$v1, 43, operatorIsPlusEvaluatePrefix	# if $v1 == 43 then operatorIsPlusEvaluatePrefix
	# -
	beq		$v1, 45, operatorIsMinusEvaluatePrefix	# if $v1 == 45 then operatorIsMinusEvaluatePrefix
	# *
	beq		$v1, 42, operatorIsAsteriskEvaluatePrefix	# if $v1 == 42 then operatorIsAsteriskEvaluatePrefix
	# /
	beq		$v1, 47, operatorIsSlashEvaluatePrefix	# if $v1 == 47 then operatorIsSlashEvaluatePrefix
	
	print_const_str "No Operator Found\n"
	# done
operatorIsPlusEvaluatePrefix:
	#print_const_str "+\n"
	add		$s5, $s4, $s3		# $s5 = $s4 + $s3
	j		pushResultToStackEvaluatePrefix				# jump to pushResultToStackEvaluatePrefix
operatorIsMinusEvaluatePrefix:
	#print_const_str "-\n"
	sub		$s5, $s4, $s3		# $s5 = $s4 - $s3
	j		pushResultToStackEvaluatePrefix				# jump to pushResultToStackEvaluatePrefix
operatorIsAsteriskEvaluatePrefix:
	#print_const_str "*\n"
	mul		$s5, $s4, $s3		# $s5 = $s4 * $s3
	j		pushResultToStackEvaluatePrefix				# jump to pushResultToStackEvaluatePrefix
operatorIsSlashEvaluatePrefix:
	#print_const_str "\\\n"
	# print
	div		$s5, $s4, $s3		# $s5 = $s4 / $s3
	# j		pushResultToStackEvaluatePrefix				# jump to pushResultToStackEvaluatePrefix
	
pushResultToStackEvaluatePrefix:	
	add		$sp, $sp, -4		# $sp = $sp + 4
	#print_const_str "Result: "
	#print_int_ln	$s5
	sw		$s5, ($sp)		# push $s5 to top of the stack	
continueLoopEvaluatePrefix:
	add		$s0, $s0, $s2		# $t0 = $t0 + $s2
	addi	$s7, $s7, -1			# $s7 = $s7 + -1
	j		loopEvaluatePrefix				# jump to loopEvaluatePrefix
endEvaluatePrefix:
	lw		$v0, ($sp)		# return top of the stack
	addi	$sp, $sp, 4
	#print_int_ln	$v0
# loopPopEvaluatePrefix:
# 	beq		$sp, $s6, endLoopPopEvaluatePrefix	# if $sp == $s6 then endLoopPopEvaluatePrefix
# 	addi	$sp, $sp, 4			# $sp = $sp + 4
# 	j		loopPopEvaluatePrefix				# jump to loopPopEvaluatePrefix
# endLoopPopEvaluatePrefix:
	loadAllS
.end_macro

.macro postFixModded(%word, %word_size)
	resetCharArray temp4
	addi	$t0, %word_size, -1 #start of postFixModded
	# print_const_str "word_size: "
	# print_int $t0
	# print_const_str "\n"
	addi $sp, $sp, -8
	sw $s2, 0($sp)	# save s2
	sw $s3, 4($sp) 	# save s3
	li	$t1, 0	# index of word
	li	$s2, 1	# numberIsNext
	add $t6, $sp, $zero
loopWordModded:
	addi $sp, $sp, -16
	sw $t0, 0($sp)
	sw $t1, 4($sp)
	sw $t6, 8($sp)
	sw $t5, 12($sp)
	# print_debug_int_ln "sp before", $sp
	getToken	%word, $t1, $s2, $s3	#s3 is size of token
	# print_debug_int_ln	"sp after", $sp
	# print_debug_int_ln "v1", $v1
	lw $t0, 0($sp)
	lw $t1, 4($sp)
	lw $t6, 8($sp)
	lw $t5, 12($sp)
	addi $sp, $sp, 16
	add $t1, $t1, $s3

	beq $v0, $zero, endLoopWordModded	#branch to end
	beq $v0, 1, OperatorModded		#branch if token is operator
	# Operand

	storeAllT
	#bltz $v1, appendNegative
	appendNumberToString $v1, temp4
	appendStringToString temp4, space
#appendNegative:
	loadAllT
	j loopWordModded

OperatorModded:
	
	# (
	beq 	$v1, 40, IsLeftBracketModded
	# )
	beq		$v1, 41, IsRightBracketModded
	# +
	beq		$v1, 43, IsComputeOpModded	# if $v1 == 43 then operatorIsPlus
	# -
	beq		$v1, 45, IsComputeOpModded	# if $v1 == 45 then operatorIsMinus
	# *
	beq		$v1, 42, IsComputeOpModded	# if $v1 == 42 then operatorIsAsterisk
	# /
	beq		$v1, 47, IsComputeOpModded	# if $v1 == 47 then operatorIsSlash

	print_const_str "No OperatorModded Found\n"
	done

IsLeftBracketModded:	# push to stack 
	addi	$sp, $sp, -4			# $sp = $sp - 4
	sw		$v1, ($sp)		# push $v1 to stack
	j loopWordModded

IsRightBracketModded:	# pop until see left bracket
loopPopModded:
	beq $t6, $sp, endLoopPopModded
	lb  $t5, 0($sp)		# pop 
	add $sp, $sp, 4			# update sp
	beq $t5, 40, endLoopPopModded
		
	addi $sp, $sp, -16
	sw $t0, 0($sp)
	sw $t1, 4($sp)
	sw $s2, 8($sp)
	sw $s3, 12($sp)
	appendCharToString $t5, temp4
	appendStringToString temp4, space
	lw $t0, 0($sp)
	lw $t1, 4($sp)
	lw $s2, 8($sp)
	lw $s3, 12($sp)
	addi $sp, $sp, 16
	j loopPopModded
endLoopPopModded:
	j loopWordModded

IsComputeOpModded:
	beq $t6, $sp, pushStackModded
loopPriorityModded:
	lb	$t5, 0($sp)
	priority $t5
	add $t5, $zero, $v0
	priority $v1
	add $t7, $zero, $v0
	bgt $t7, $t5, pushStackModded	# if $t7 > $t5 then pushStackModded
	lb $t5, 0($sp)
	addi $sp, $sp, 4
	
	addi $sp, $sp, -16
	sw $t0, 0($sp)
	sw $t1, 4($sp)
	sw $s2, 8($sp)
	sw $s3, 12($sp)
	appendCharToString $t5, temp4
	appendStringToString temp4, space
	lw $t0, 0($sp)
	lw $t1, 4($sp)
	lw $s2, 8($sp)
	lw $s3, 12($sp)
	addi $sp, $sp, 16
	j loopPriorityModded	
pushStackModded:
	addi $sp, $sp, -4
	sw $v1, 0($sp)
	j loopWordModded

endLoopWordModded:
loopPopModded_final:
	beq $t6, $sp, endLoopPopModded_final
	lb $t5, 0($sp)		# pop 
	add $sp, $sp, 4		# update sp
	beq $t5, 40, endLoopPopModded_final
	
	addi $sp, $sp, -16
	sw $t0, 0($sp)
	sw $t1, 4($sp)
	sw $s2, 8($sp)
	sw $s3, 12($sp)
	appendCharToString $t5, temp4
	appendStringToString temp4, space
	lw $t0, 0($sp)
	lw $t1, 4($sp)
	lw $s2, 8($sp)
	lw $s3, 12($sp)
	addi $sp, $sp, 16
	j loopPopModded_final
endLoopPopModded_final:
	lw $s2, 0($sp) # load s2
	lw $s3, 4($sp) # load s3
	addi $sp, $sp, 8
	# print_debug_int_ln "sp before", $sp
	trimSpace temp4
	# print_debug_int_ln "sp after", $sp

	# storeAllS
	# appendStringToString %result, temp4
	# print_str_ln 	temp4
	# evaluatePostfix temp4
	# print_const_str "result: "
	# print_int_ln $v0
	# appendNumberToString $v0, result
	# appendStringToString result, endLine
.end_macro

.macro preFix(%expression, %resultExp, %result)
	getSize %expression
	postFixModded %expression, $v0
	evaluatePostfix temp4
	appendNumberToString $v0, result
	# print_debug_int_ln	"sp: ", $sp
	# print_int_ln	$sp
	# print_int_ln	$s0
	# print_int_ln	$s2
	# print_endl
	# loadAllS
    appendStringToString    result, endLine
	# print_int_ln	$s0
	# print_int_ln	$s2
	# print_debug_int_ln	"sp", $sp
    addi	$sp, $sp, -4			# $sp = $sp + -4        
    sw		$s0, 0($sp)		# ^(*&^*(&%^&^$(*%)&^(*^&(*
	# print_int_ln	$s2
    print_str_ln	%expression
     charToInt %expression, intArray, isNumber
	# print_str_ln		%expression
    # printIntArray   intArray, isNumber
    # reverseString   	isNumber, isNumberReversed
	# printIntArray	intArray, isNumber
    # reverseWordString   intArray, intArrayReversed
	# printIntArray		intArrayReversed, isNumberReversed
	# resetCharArray	%expression
	reverseString	%expression, reversed
    # intToChar   intArrayReversed, isNumberReversed, %expression
	# printIntArray		intArrayReversed, isNumberReversed
	print_str_ln	reversed
	# print_endl
    switchBracket reversed

	# printIntArray	intArrayReversed, isNumberReversed
	print_str_ln	reversed
	# print_endl
	# print_debug_int_ln	"sp: ", $sp
    getSize reversed
    addi	$s0, $v0, 0			# $s0 = $v0 + 0
	# print_int_ln	$sp
	# print_debug_int_ln	"$v0", $v0
	print_str_ln	reversed
    postFixModded reversed, $s0
	# print_debug_int_ln	"sp: ", $sp
	# print_int_ln	$sp
	# print_int_ln	$s2
	print_str_ln	temp4
	# print_endl
	lw		$s0, ($sp)
	addi	$sp, $sp, 4			# $sp = $sp + 4
	# print_debug_int_ln	"sp: ", $sp
	print_str_ln	temp4
	# print_int_ln	$sp
    # evaluatePrefix temp4
	# print_int_ln	$sp
	# print_debug_int_ln	"sp: ", $sp
	print_debug_int_ln	"result", $v0
	# storeAllS
	# print_int_ln	$sp
    
	# print_debug_int_ln	"sp: ", $sp
	print_str_ln		temp4
    # charToIntWithSpace           temp4, intArray, isNumber
	# printIntArray		intArray, isNumber
	# print_debug_int_ln	"sp: ", $sp
    # reverseWordString   intArray, intArrayReversed
    # reverseString       isNumber, isNumberReversed
	# printIntArray		intArrayReversed, isNumberReversed
    # intToCharWithSpace			intArrayReversed, isNumberReversed, %expression
	reverseString	temp4, %expression
	print_str_ln				%expression
	print_endl
	# print_debug_int_ln	"sp: ", $sp
    appendStringToString        %result, %expression
    appendStringToString        %result, endLine
	# print_debug_int_ln	"sp: ", $sp
.end_macro
