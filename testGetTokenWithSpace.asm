.include "macros.asm"
.include "postfix.asm"

.data
	input_loc: 		.asciiz	"input.txt"
	post_output_loc:	.asciiz "postfix.txt"
	pre_output_loc:		.asciiz "prefix.txt"
	result_output_loc:	.asciiz "result.txt"
	prefix: 		.asciiz	"Prefix"
	postfix:		.asciiz	"Postfix"
	endLine:		.asciiz "\n"
	space:			.asciiz " "
	prefixResult:  	.space 	1024
	postfixResult: 	.space 	1024
	result:		.space	1024
	buffer: 		.space 	1024
	word:			.space 	1024
	reversed:		.space 	1024
	temp1:			.space 	1024
	temp2:			.space 	1024
	temp3:			.space 	1024
	temp4:			.space  1024
	intArray:			.word	1024
	isNumberReversed:	.space	1024
	intArrayReversed:		.word	1024
	temp5:			.space	1024
	isNumber:		.byte	1024
	test: 		.asciiz "(66*7-3-2)/55-55*(3+2)"
.text
	li		$s0, 0
	li 		$s1, 1
	li 		$s2, 1
	print_str_ln test
	#getSize test
	#addi $s3, $v0, 0
	#print_int_ln $s3
	postFix test, $s3, postfixResult
	#print_int_ln $s3
	print_str_ln postfixResult
	#evaluatePostfix test
	j endProgram
loop:
	#print_const_str "index: "
	#print_int_ln	$s0
	getToken	test, $s0, $s1, $s2
	beq		$v0, $zero, end	# if $v0 == $zero then end
	#print_int_ln	$v0
	print_const_str "value: "
	print_int_ln	$v1
	print_const_str "type: "
	print_int_ln	$v0
	print_const_str "size: "
	print_int_ln	$s2
	print_const_str "number is next: "
	print_int_ln	$s1
	
	add		$s0, $s0, $s2		# $s0 = $s0 + $s2
	j		loop				# jump to loop
end:
	j		endProgram				# jump to endProgram

postfixSection:	
	print_const_str	"Postfix\n"
	
loopPostfix:
	lb	$s1, 0($s0)	# load buffer to s1
	beq	$s1, $zero, endLoopPostfix	# check condition
	li	$s2, 0
	getWord $s0, word, $s2 # word and s2 = length of word
	print_str_ln	word
	add	$s0, $s0, $s2 # update buffer
	li	$s1, 1
	li	$s3, 1
	li	$s5, 1
loopWord:
	getToken word, $s1, $s5, $s4
	#j loopPostfix
	print_int_ln	$v1
	beq	$v0, $zero, endLoopWord
	add $s1, $s1, $s4
	j loopWord
endLoopWord:
endLoopPostfix:
	j	endProgram
	
	
########### PREFIX #############
	
prefixSection:
	print_const_str	"Prefix"
	
loopPrefix:
	lb	$s1, 0($s0)
	beq	$s1, $zero, endLoopPrefix
	li	$s2, 0
	getWord $s0, word, $s2
	#print_str_ln	word
	add	$s0, $s0, $s2
	j loopPrefix
endLoopPrefix:
	j	endProgram

endProgram:
	li $v0,10
	syscall
	
############EQUAL_STRING##################
equalString:
	addi	$t0, $a0, 0
	addi	$t1, $a1, 0
loopEqualString:
	lb	$t2, 0($t0)	# t2 = a0[i]
	lb	$t3, 0($t1)	# t3 = a1[i]
	
	bne	$t2, $t3, notEqual
	
	sltiu	$t4, $t2, 1	# t4 = 1 if t2 == null
	sltiu	$t5, $t3, 1	# t5 = 1 if t3 == null
	
	and	$t6, $t4, $t5
	bne	$t6, $zero, equal
	
	addi	$t0, $t0, 1
	addi	$t1, $t1, 1
	j	loopEqualString
	
equal:
	addi	$v0, $zero, 0
	j	endFunction
notEqual:
	addi	$v0, $zero, 0
	j	endFunction
endFunction:
	jr	$ra
#################################
