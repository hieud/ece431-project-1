.macro evaluatePostfix(%expression)
	storeAllS
	li		$s0, 0		# load ADD[expression] 
	# add		$s3, $sp, $zero		# $s3 = original $sp
	li		$s1, 1		# $s1 = 1
	li		$s2, 1		# $s2 = 1
loopEvaluatePostfix:
	getToken	%expression, $s0, $s1, $s2
	# print_const_str "value: "
	# print_int_ln	$v1
	# print_const_str "type: "
	# print_int_ln	$v0
	# print_const_str "size: "
	# print_int_ln	$s2
	# print_const_str "number is next: "
	# print_int_ln	$s1
	beq		$v0, $zero, endEvaluatePostfix	# if $v0 == $zero then endEvaluatePostfix
	beq		$v0, 1, tokenIsOperator	# if $v0 == 1 then tokenIsOperator
	j		tokenIsOperand				# jump to tokenIsOperand
tokenIsOperand:
	#print_const_str	"Operand\n"
	addi	$sp, $sp, -4			# $sp = $sp - 4
	sw		$v1, ($sp)		# push $v1 to stack
	j 	continueLoop
tokenIsOperator:
	#print_const_str	"Operator\n"
	lw		$s3, ($sp)		# $s3 = b
	lw		$s4, 4($sp)		# $s4 = a
	addi	$sp, $sp, 8			# $sp = $sp + 8
	
	# +
	beq		$v1, 43, operatorIsPlus	# if $v1 == 43 then operatorIsPlus
	# -
	beq		$v1, 45, operatorIsMinus	# if $v1 == 45 then operatorIsMinus
	# *
	beq		$v1, 42, operatorIsAsterisk	# if $v1 == 42 then operatorIsAsterisk
	# /
	beq		$v1, 47, operatorIsSlash	# if $v1 == 47 then operatorIsSlash
	
	print_const_str "No Operator Found\n"
	done
operatorIsPlus:
	#print_const_str "+\n"
	add		$s5, $s4, $s3		# $s5 = $s4 + $s3
	j		pushResultToStack				# jump to pushResultToStack
operatorIsMinus:
	#print_const_str "-\n"
	sub		$s5, $s4, $s3		# $s5 = $s4 - $s3
	j		pushResultToStack				# jump to pushResultToStack
operatorIsAsterisk:
	#print_const_str "*\n"
	mul		$s5, $s4, $s3		# $s5 = $s4 * $s3
	j		pushResultToStack				# jump to pushResultToStack
operatorIsSlash:
	#print_const_str "\\\n"
	div		$s5, $s4, $s3		# $s5 = $s4 / $s3
pushResultToStack:	
	add		$sp, $sp, -4		# $sp = $sp + 4
	#print_const_str "Result: "
	#print_int_ln	$s5
	sw		$s5, ($sp)		# push $s5 to top of the stack	
continueLoop:
	add		$s0, $s0, $s2		# $t0 = $t0 + $s2
	j		loopEvaluatePostfix				# jump to loopEvaluatePostfix
endEvaluatePostfix:
	lw		$v0, ($sp)		# return top of the stack
	addi	$sp, $sp, 4
	#print_int_ln	$v0
	loadAllS
.end_macro

.macro postFix(%word, %word_size, %result)
	resetCharArray temp4
	addi	$t0, %word_size, -1 #start of postFix
	print_const_str "word_size: "
	print_int $t0
	print_const_str "\n"
	addi $sp, $sp, -8
	sw $s2, 0($sp)	# save s2
	sw $s3, 4($sp) 	# save s3
	li	$t1, 0	# index of word
	li	$s2, 1	# numberIsNext
	add $t6, $sp, $zero
loopWord:
	addi $sp, $sp, -16
	sw $t0, 0($sp)
	sw $t1, 4($sp)
	sw $t6, 8($sp)
	sw $t5, 12($sp)
	getToken	%word, $t1, $s2, $s3	#s3 is size of token
	lw $t0, 0($sp)
	lw $t1, 4($sp)
	lw $t6, 8($sp)
	lw $t5, 12($sp)
	addi $sp, $sp, 16
	add $t1, $t1, $s3

	beq $v0, $zero, endLoopWord	#branch to end
	beq $v0, 1, Operator		#branch if token is operator
	# Operand
	storeAllT
	#bltz $v1, appendNegative
	appendNumberToString $v1, temp4
	appendStringToString temp4, space
#appendNegative:
	loadAllT
	j loopWord

Operator:
	
	# (
	beq 	$v1, 40, IsLeftBracket
	# )
	beq		$v1, 41, IsRightBracket
	# +
	beq		$v1, 43, IsComputeOp	# if $v1 == 43 then operatorIsPlus
	# -
	beq		$v1, 45, IsComputeOp	# if $v1 == 45 then operatorIsMinus
	# *
	beq		$v1, 42, IsComputeOp	# if $v1 == 42 then operatorIsAsterisk
	# /
	beq		$v1, 47, IsComputeOp	# if $v1 == 47 then operatorIsSlash

	print_const_str "No Operator Found\n"
	done

IsLeftBracket:	# push to stack 
	addi	$sp, $sp, -4			# $sp = $sp - 4
	sw		$v1, ($sp)		# push $v1 to stack
	j loopWord

IsRightBracket:	# pop until see left bracket
loopPop:
	beq $t6, $sp, endLoopPop
	lb  $t5, 0($sp)		# pop 
	add $sp, $sp, 4			# update sp
	beq $t5, 40, endLoopPop
	addi $sp, $sp, -16
	sw $t0, 0($sp)
	sw $t1, 4($sp)
	sw $s2, 8($sp)
	sw $s3, 12($sp)
	appendCharToString $t5, temp4
	appendStringToString temp4, space
	lw $t0, 0($sp)
	lw $t1, 4($sp)
	lw $s2, 8($sp)
	lw $s3, 12($sp)
	addi $sp, $sp, 16
	j loopPop
endLoopPop:
	j loopWord

IsComputeOp:
	beq $t6, $sp, pushStack
loopPriority:
	lb	$t5, 0($sp)
	priority $t5
	add $t5, $zero, $v0
	priority $v1
	add $t7, $zero, $v0
	bgt $t7, $t5, pushStack	# if $t7 > $t5 then pushStack
	lb $t5, 0($sp)
	addi $sp, $sp, 4
	
	addi $sp, $sp, -16
	sw $t0, 0($sp)
	sw $t1, 4($sp)
	sw $s2, 8($sp)
	sw $s3, 12($sp)
	appendCharToString $t5, temp4
	appendStringToString temp4, space
	lw $t0, 0($sp)
	lw $t1, 4($sp)
	lw $s2, 8($sp)
	lw $s3, 12($sp)
	addi $sp, $sp, 16
	j loopPriority	
pushStack:
	addi $sp, $sp, -4
	sw $v1, 0($sp)
	j loopWord

endLoopWord:
loopPop_final:
	beq $t6, $sp, endLoopPop_final
	lb $t5, 0($sp)		# pop 
	add $sp, $sp, 4		# update sp
	beq $t5, 40, endLoopPop_final
	
	addi $sp, $sp, -16
	sw $t0, 0($sp)
	sw $t1, 4($sp)
	sw $s2, 8($sp)
	sw $s3, 12($sp)
	appendCharToString $t5, temp4
	appendStringToString temp4, space
	lw $t0, 0($sp)
	lw $t1, 4($sp)
	lw $s2, 8($sp)
	lw $s3, 12($sp)
	addi $sp, $sp, 16
	j loopPop_final
endLoopPop_final:
	lw $s2, 0($sp) # load s2
	lw $s3, 4($sp) # load s3
	addi $sp, $sp, 8
	trimSpace temp4
	appendStringToString %result, temp4
	print_str_ln 	temp4
	evaluatePostfix temp4
	print_const_str "result: "
	print_int_ln $v0
	appendNumberToString $v0, result
	appendStringToString result, endLine
.end_macro

.macro priority(%a)
beq %a, 43, plus_minus
beq %a, 45, plus_minus
beq %a, 42, aster_slash
beq %a, 47, aster_slash
li $v0, -1
j endPriority

plus_minus:
li $v0, 0
j endPriority

aster_slash:
li $v0, 1
j endPriority

endPriority:
.end_macro

